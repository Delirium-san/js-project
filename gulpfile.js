/* eslint-disable node/no-unpublished-require */
const gulp = require("gulp");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const cssnano = require("gulp-cssnano");
const nodemon = require("gulp-nodemon");
const livereload = require("gulp-livereload");
const concat = require("gulp-concat");
const uglify = require("gulp-uglifyjs");
/* eslint-enable node/no-unpublished-require */

// Compact all scss
gulp.task("scss", function() {
  return gulp
    .src("dev/scss/**/*.scss")
    .pipe(sass())
    .pipe(
      autoprefixer(["last 15 versions", "> 1%", "ie 8", "ie 7"], {
        cascade: true
      })
    )
    .pipe(cssnano())
    .pipe(gulp.dest("public/stylesheets"))
    .pipe(livereload({ auto: true }));
});

// If anything change - RESTART
gulp.task("watch", function() {
  gulp.watch("dev/scss/**/*.scss", gulp.series("scss"));
  gulp.watch("dev/js/**/*.js", gulp.series("scripts"));
});

// Compact all scripts
gulp.task("scripts", function() {
  return gulp
    .src([
      "dev/js/auth.js",
      "dev/js/post.js",
      "node_modules/medium-editor/dist/js/medium-editor.min.js"
    ])
    .pipe(concat("scripts.js"))
    .pipe(uglify())
    .pipe(gulp.dest("public/javascripts"));
});

// Only way to comnine nodemon and gulp
gulp.task("nodemon", cb => {
  let started = false;
  return nodemon({
    script: "app.js"
  }).on("start", () => {
    if (!started) {
      cb();
      started = true;
    }
  });
});

gulp.task("default", gulp.parallel("watch", "nodemon", "scripts"));
