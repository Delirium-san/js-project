/* eslint-disable no-undef */
$(function() {
  // eslint-disable-next-line
  var editor = new MediumEditor("#post-body", {
    placeholder: {
      text: "",
      hideOnClick: true
    }
  });

  // publish
  $(".publish-button").on("click", function(e) {
    e.preventDefault();

    var data = {
      title: $("#post-title").val(),
      body: $("#post-body").html()
    };

    $.ajax({
      type: "POST",
      data: JSON.stringify(data),
      contentType: "application/json",
      url: "/post/add"
    }).done(function(data) {
      if (!data.ok) {
        $(".post-form h2").after('<p class="error">' + data.error + "</p>");
        if (data.fields) {
          data.fields.forEach(function(item) {
            $("#post-" + item).addClass("error");
          });
        }
      } else {
        $(location).attr("href", "/");
      }
    });
  });
});

/* eslint-enable no-undef */
