// Fill 30 posts with random content (only in start)

const faker = require("faker/locale/en_US");
const TurndownService = require("turndown");

const models = require("./models");

const owner = "5df67aff52ee8b03dc36084d";

module.exports = () => {
  models.Post.remove()
    .then(() => {
      Array.from({ length: 30 }).forEach(() => {
        const turndownService = new TurndownService();

        models.Post.create({
          title: faker.hacker.phrase(),
          body: turndownService.turndown(faker.random.words(55)),
          owner
        })
          .then(console.log)
          .catch(console.log);
      });
    })
    .catch(console.log);
};
