// Agregate all models

const Post = require("./post");
const User = require("./userOfDb");

module.exports = {
  Post,
  User
};
