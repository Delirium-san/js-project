const dotenv = require("dotenv");
const path = require("path");

const root = path.join.bind(this, __dirname);
dotenv.config({ path: root(".env") });

module.exports = {
  PORT: process.env.PORT === undefined ? 3000 : process.env.PORT,
  IS_PROD: process.env.NODE_ENV === "production",
  SESSION_SECRET: process.env.SESSION_SECRET,
  PER_PAGE: process.env.PER_PAGE,
  MONGO_URL:
    "mongodb+srv://AdminUser:somePass@cluster-b6dk0.mongodb.net/base?retryWrites=true&w=majority"
};
