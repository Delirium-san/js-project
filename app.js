const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const config = require("./config");
const routes = require("./routers");
const mongoose = require("mongoose");
const staticAsset = require("static-asset");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
//const mocks = require("./mocks");

// Mongoose Db
mongoose.Promise = global.Promise;
mongoose.set("debug", config.IS_PROD);
mongoose.connection
  .on("error", error => console.log(error))
  .on("close", () => console.log("Dstabase connection closed."))
  .once("open", () => {
    const info = mongoose.connections[0];
    console.log(`Connected to ${info.host}:${info.port}/${info.name}`);
    //mocks(); // use to fill db
  });
mongoose.connect(config.MONGO_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});

const app = express();

// Get our session
app.use(
  session({
    secret: config.SESSION_SECRET,
    resave: true,
    saveUnitialized: false,
    store: new MongoStore({
      mongooseConnection: mongoose.connection
    })
  })
);
app.use(function(req, res, next) {
  res.locals.userLogin = req.session.userLogin;
  next();
});

// Because of using EJS
app.set("view engine", "ejs");

// Useful modules
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(staticAsset(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "public")));
app.use(
  "/javascripts",
  express.static(path.join(__dirname, "node_modules", "jquery", "dist"))
);

// Routes mapping
app.use("/", routes.archieve);
app.use("/api/auth", routes.auth);
app.use("/post", routes.post);

// Ugly error handling
app.use((req, res, next) => {
  const err = new Error("Not Found!");
  err.status = 404;
  next(err);
});

// Nice error handling
// eslint-disable-next-line no-unused-vars
// app.use((error, req, res, next) => {
//   res.status(error.status || 500);
//   res.render("error", {
//     message: error.message,
//     error: !config.IS_PROD ? error : {}
//   });
// });

// Listen
app.listen(config.PORT, () =>
  console.log(`Example app listening on port ${config.PORT}!`)
);

module.exports = routes;
