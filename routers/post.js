const express = require("express");
const router = express.Router();
const TurndownService = require("turndown");
const mongodb = require("mongodb");

const models = require("../models");

// Go to Create Post page
router.get("/add", (req, res) => {
  const userId = req.session.userId;
  const userLogin = req.session.userLogin;

  if (!userId || !userLogin) {
    res.redirect("/");
  } else {
    res.render("post/add", {
      user: {
        id: userId,
        login: userLogin
      }
    });
  }
});

// Create new post if everything ok
router.post("/add", (req, res) => {
  const userId = req.session.userId;
  const userLogin = req.session.userLogin;

  if (!userId || !userLogin) {
    res.redirect("/");
  } else {
    const title = req.body.title.trim();
    const body = req.body.body;
    const turndownService = new TurndownService();

    if (!title || !body) {
      const fields = [];
      if (!title) fields.push("title");
      if (!body) fields.push("body");

      res.json({
        ok: false,
        error: "Все поля должны быть заполнены!",
        fields
      });
    } else {
      models.Post.create({
        title,
        body: turndownService.turndown(body),
        owner: userId
      })
        .then(post => {
          console.log(post);
          res.json({
            ok: true
          });
        })
        .catch(err => {
          console.log(err);
          res.json({
            ok: false
          });
        });
    }
  }
});

// Delete post after click Delete link
router.get("/delete/:id", (req, res) => {
  const userId = req.session.userId;
  const userLogin = req.session.userLogin;
  const id = req.params.id.trim();

  if (!userId || !userLogin) {
    res.redirect("/");
  } else {
    models.Post.deleteOne({
      _id: new mongodb.ObjectID(id)
    })
      .then(post => {
        console.log(post);
        res.redirect("/");
      })
      .catch(err => {
        console.log(err);
        res.json({
          ok: false
        });
      });
  }
});

module.exports = router;
