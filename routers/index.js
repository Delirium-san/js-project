// Combo of all routers

const auth = require("./auth");
const post = require("./post");
const archieve = require("./archieve");

module.exports = {
  auth,
  post,
  archieve
};
