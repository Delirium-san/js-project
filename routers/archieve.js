const express = require("express");
var moment = require("moment");
const router = express.Router();
const config = require("../config");
const models = require("../models");

// Funbction to get posts from DB
function posts(req, res) {
  const userId = req.session.userId;
  const userLogin = req.session.userLogin;
  const perPage = +config.PER_PAGE;
  const page = req.params.page || 1;

  models.Post.find({})
    .skip(perPage * page - perPage)
    .limit(perPage)
    .populate("owner")
    .sort({ createdAt: -1 })
    .then(posts => {
      models.Post.count().then(count => {
        res.render("archieve/index", {
          posts,
          current: page,
          pages: Math.ceil(count / perPage),
          user: {
            id: userId,
            login: userLogin
          },
          moment: moment
        });
      });
    })
    .catch(console.log);
}
router.get("/", (req, res) => posts(req, res));
router.get("/archive/:page", (req, res) => posts(req, res));

// Get page of specified post
router.get("/posts/:post", (req, res, next) => {
  const url = req.params.post.trim();
  const userId = req.session.userId;
  const userLogin = req.session.userLogin;

  if (!url) {
    const err = new Error("Not Found");
    err.status = 404;
    next(err);
  } else {
    models.Post.findOne({
      url
    })
      .populate("owner")
      .then(post => {
        if (!post) {
          const err = new Error("Not Found");
          err.status = 404;
          next(err);
        } else {
          res.render("post/post", {
            post,
            user: {
              id: userId,
              login: userLogin
            }
          });
        }
      });
  }
});

// Get post of specified user
router.get("/users/:login/:page*?", (req, res) => {
  const userId = req.session.userId;
  const userLogin = req.session.userLogin;
  const perPage = +config.PER_PAGE;
  const page = req.params.page || 1;
  const login = req.params.login;

  models.User.findOne({
    login
  }).then(user => {
    models.Post.find({
      owner: user.id
    })
      .skip(perPage * page - perPage)
      .limit(perPage)
      .populate("owner")
      .sort({ createdAt: -1 })
      .then(posts => {
        models.Post.count({
          owner: user.id
        }).then(count => {
          res.render("archieve/user", {
            posts,
            _user: user,
            current: page,
            pages: Math.ceil(count / perPage),
            user: {
              id: userId,
              login: userLogin
            },
            moment: moment
          });
        });
      })
      .catch(() => {
        throw new Error("Server Error");
      });
  });
});

module.exports = router;
